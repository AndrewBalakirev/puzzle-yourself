from kivymd.app import MDApp

from src.Model.menu_screen.components import MenuItem
from src.Model.mixins import ModelMixin
from src.View.MenuScreen.menu_screen import MenuScreenView


class MenuScreenController:
    def __init__(self, model: ModelMixin) -> None:
        self.model = model
        self.view = MenuScreenView(controller=self, model=self.model)

    def get_view(self) -> MenuScreenView:
        return self.view

    def on_click(self, action, **attrs) -> None:
        if action.startswith('go_to_._'):
            action, menu_name = action.split('_._')
            attrs.update(name=menu_name)

        action_func = getattr(self, f'action_{action}')
        if action_func:
            action_func(**attrs)

    def action_start_game(self) -> None:
        self.view.manager.current = 'game screen'

    def action_go_to(self, name) -> None:
        self.model.go_to(name)

    def action_quit(self) -> None:
        app = MDApp.get_running_app()
        app.stop()

    def get_menu_items(self) -> list[MenuItem]:
        return self.model.get_menu_items()
