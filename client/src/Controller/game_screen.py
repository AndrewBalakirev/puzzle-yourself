from src.Model.mixins import ModelMixin
from src.View.GameScreen.game_screen import GameScreenView


class GameScreenController:
    def __init__(self, model: ModelMixin) -> None:
        self.model = model
        self.view = GameScreenView(controller=self, model=self.model)

    def get_view(self) -> GameScreenView:
        return self.view
