from src.Utility.observer import Observer


class ModelMixin:
    _observers: list[Observer]

    def __init_subclass__(cls):
        cls._observers: list[Observer] = []

    def notify_observers(self) -> None:
        """
        The method that will be called on the observer when the model changes.
        """
        for observer in self._observers:
            observer.model_is_changed()

    def add_observer(self, observer: Observer) -> None:
        self._observers.append(observer)

    def remove_observer(self, observer: Observer) -> None:
        self._observers.remove(observer)
