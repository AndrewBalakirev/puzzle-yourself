from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional, Union


if TYPE_CHECKING:
    from .menu_group import MenuClassNamedGroup


@dataclass
class MenuItem:
    name: str
    action: str
    go_to: Optional[str] = None

    def __post_init__(self) -> None:
        if self.go_to is not None:
            self.action = f'go_to_._{self.go_to}'


def menu_item(name: str, action: str) -> MenuItem:
    return MenuItem(name=name, action=action)


def go_to_menu(name: str, go_to: Union[str, 'MenuClassNamedGroup']) -> MenuItem:
    if not isinstance(go_to, str):
        go_to = go_to.class_name_to_snake_case()
    return MenuItem(name=name, action='go_to', go_to=go_to)
