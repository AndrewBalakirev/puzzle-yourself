import re
from dataclasses import dataclass, field
from typing import Optional

from .menu_item import MenuItem


@dataclass
class MenuGroup:
    items: list[MenuItem]
    name: str


@dataclass
class MenuClassNamedGroup(MenuGroup):
    name: Optional[str] = field(init=False, default=None)
    items: list[MenuItem] = field(init=False, default_factory=list)

    def __post_init__(self) -> None:
        self.name = self.class_name_to_snake_case()
        self.init_items()

    @classmethod
    def class_name_to_snake_case(cls) -> str:
        name = cls.__name__
        pattern = re.compile(r'(?<!^)(?=[A-Z])')
        return pattern.sub('_', name).lower()

    def init_items(self) -> None:
        pass
