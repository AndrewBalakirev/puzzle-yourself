from typing import Iterable
from src.Model.mixins import ModelMixin

from .components import MenuClassNamedGroup, MenuGroup, MenuItem, menu_item


class MainMenuGroup(MenuClassNamedGroup):
    def init_items(self):
        self.items = [
            menu_item(name='Играть', action='start_game'),
            menu_item(name='Выход', action='quit'),
        ]


class MenuScreenModel(ModelMixin):
    _initial_group = MainMenuGroup
    _menu_groups: Iterable[MenuGroup] = [
        MainMenuGroup(),
    ]

    def __init__(self) -> None:
        initial_group_name = self._initial_group.class_name_to_snake_case()
        self._current_items: Iterable[MenuItem] = self._get_menu_group(initial_group_name).items

    def get_menu_items(self) -> Iterable[MenuItem]:
        return self._current_items

    def go_to(self, name: str) -> None:
        self._current_items = self._get_menu_group(name).items
        self.notify_observers()

    def _get_menu_group(self, name: str) -> MenuGroup:
        try:
            return next(filter(lambda x: x.name == name, self._menu_groups))
        except StopIteration:
            raise ValueError(f'MenuGroup with {name=} is not found')
