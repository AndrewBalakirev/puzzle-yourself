from src.Controller.game_screen import GameScreenController
from src.Controller.menu_screen import MenuScreenController
from src.Model.game_screen import GameScreenModel
from src.Model.menu_screen import MenuScreenModel


screens = {
    'menu screen': {
        'model': MenuScreenModel,
        'controller': MenuScreenController,
    },
    'game screen': {
        'model': GameScreenModel,
        'controller': GameScreenController,
    },
}
