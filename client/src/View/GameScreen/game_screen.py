from collections.abc import Sequence
import filetype
from kivymd.uix.screen import MDScreen

from src.Utility.observer import Observer
from src.Utility.slicer import Slicer
from src.View.GameScreen.components import TileImage


class GameScreenView(Observer, MDScreen):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def select(self, f: str, filepaths: Sequence[str]) -> None:
        filepath = filepaths[0] if filepaths else None
        if filepath and filetype.is_image(filepath):
            tiles = Slicer(filepath).slice(16)
            self.ids.ff.clear_widgets()
            for tile in tiles:
                im = TileImage(
                    move_cb=self.move_cb,
                    touch_up_cb=self.touch_up_cb,
                    tile=tile
                )
                self.ids.ff.add_widget(im)

    def get_tile_by_position(self, pos) -> TileImage:
        for tile in self.ids.ff.children:
            if tile.position == pos:
                return tile

        raise ValueError("Tile not found")

    def move_cb(self, touch):
        return True

    def touch_up_cb(self, touch):
        current_tile = touch.grab_current
        x, y = current_tile.x, current_tile.y
        aviliable_gap = 3
        for tile in self.ids.ff.children:
            if tile == current_tile:
                continue
            tile_x, tile_y = tile.x, tile.y
            print('x', tile_x, x+aviliable_gap)
            print('y', tile_y, y+aviliable_gap)
        return True

    def model_is_changed(self):
        pass
