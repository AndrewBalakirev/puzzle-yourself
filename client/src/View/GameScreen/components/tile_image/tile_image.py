from io import BytesIO

from kivy.core.image import Image as CoreImage
from kivy.properties import DictProperty, ListProperty, NumericProperty, ObjectProperty
from kivy.uix.behaviors import DragBehavior
from kivy.uix.image import Image


class TileImage(DragBehavior, Image):
    move_cb = ObjectProperty()
    touch_up_cb = ObjectProperty()
    tile = ObjectProperty()
    number = NumericProperty()
    neighbors = DictProperty({})
    position = ListProperty()

    def on_touch_move(self, touch):
        super().on_touch_move(touch)
        if touch.grab_current is not self:
            return True

        uid = self._get_uid()
        ud = touch.ud[uid]
        mode = ud['mode']
        if mode == 'drag' and callable(self.move_cb):
            self.move_cb(touch)

        return True

    def on_touch_up(self, touch):
        super().on_touch_up(touch)
        if touch.grab_current is not self:
            return True

        if callable(self.touch_up_cb):
            self.touch_up_cb(touch)

        return True

    def on_tile(self, _, tile):
        self.set_image(tile.image)
        self.neighbors = tile.neighbors
        self.number = tile.number
        self.position = tile.position

    def set_image(self, tile_image) -> None:
        data = BytesIO()
        tile_image.save(data, format='png')
        data.seek(0)
        core_image = CoreImage(data, ext='png')
        self.texture = core_image.texture
