from kivymd.uix.screen import MDScreen

from src.Utility.observer import Observer
from src.View.MenuScreen.components import MenuButton


class MenuScreenView(Observer, MDScreen):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.draw_menu()

    def draw_menu(self) -> None:
        self.ids.menu.clear_widgets()
        menu_items = self.controller.get_menu_items()
        for menu_item in menu_items:
            self.ids.menu.add_widget(
                MenuButton(
                    text=menu_item.name,
                    action=menu_item.action,
                    on_release=self.button_on_click
                )
            )

    def button_on_click(self, instance: MenuButton) -> None:
        self.controller.on_click(instance.action)

    def model_is_changed(self) -> None:
        self.draw_menu()
