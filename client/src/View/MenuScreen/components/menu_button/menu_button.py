from kivy.properties import StringProperty
from kivy.uix.button import Button


class MenuButton(Button):
    action = StringProperty()
