from kivy.properties import ObjectProperty


class Observer:
    """Abstract superclass for all observers."""

    model = ObjectProperty()
    controller = ObjectProperty()
    initalized = False

    def on_model(self, instance, model):
        if self.initalized:
            return

        model.add_observer(instance)
        self.initalized = True

    def model_is_changed(self):
        raise NotImplementedError
