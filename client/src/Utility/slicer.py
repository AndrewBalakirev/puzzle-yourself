from dataclasses import dataclass
from math import ceil, floor, sqrt

from PIL import Image
from PIL.Image import Image as ImageType


@dataclass
class Tile:
    """Represents a single tile."""

    image: ImageType
    number: int
    position: tuple[int, int]
    coords: tuple[int, int]
    neighbors: dict[str, tuple[int, int]]

    def __repr__(self) -> str:
        """Show tile number, and if saved to disk, filename."""
        return f'<Tile #{self.number}>'

    @property
    def row(self) -> int:
        return self.position[0]

    @property
    def column(self) -> int:
        return self.position[1]


class Slicer:
    """
    Утилита для разделения изображения на несколько
    """

    def __init__(self, filepath: str) -> None:
        self.image = Image.open(filepath)
        self.width, self.height = self.image.size

    def slice(self, count_tiles: int) -> list[Tile]:
        """
        Разделяет изображение на несколько частей равное(почти) count_tiles.

        params:
            count_tiles: int - количество частей(если нечетное,
                                                 то меняется в большую сторону до четного)
        """

        self.__validate_count_tiles(count_tiles)

        rows, columns = self.get_rows_columns(count_tiles)
        tile_height, tile_width = self.__get_tile_size(rows, columns)

        tiles: list[Tile] = []
        number = 1

        for height_pos in range(0, self.height - rows, tile_height):
            for width_pos in range(0, self.width - columns, tile_width):
                box = (
                    width_pos,
                    height_pos,
                    width_pos + tile_width,
                    height_pos + tile_height
                )
                tile_imgage = self.image.crop(box=box)
                position = (
                    int(floor(width_pos / tile_width)) + 1,
                    int(floor(height_pos / tile_height)) + 1
                )
                coords = (width_pos, height_pos)
                neighbors = self.calc_neighbors(rows, columns, position)
                tile = Tile(
                    image=tile_imgage,
                    number=number,
                    coords=coords,
                    position=position,
                    neighbors=neighbors
                )
                tiles.append(tile)
                number += 1

        return tiles

    def calc_neighbors(self, rows: int, columns: int, pos: tuple) -> dict[str, tuple[int, int]]:
        row = 0
        column = 1
        neighbors = {}
        if pos[row] != 1:
            neighbors['up'] = (pos[row] - 1, pos[column])
        if pos[row] != rows:
            neighbors['down'] = (pos[row] + 1, pos[column])

        if pos[column] != 1:
            neighbors['left'] = (pos[row], pos[column] - 1)
        if pos[column] != columns:
            neighbors['right'] = (pos[row], pos[column] + 1)
        print(neighbors)
        return neighbors

    def get_rows_columns(self, count_tiles: int) -> tuple[int, int]:
        columns = int(ceil(sqrt(count_tiles)))
        rows = int(ceil(count_tiles / float(columns)))
        return rows, columns

    def __validate_count_tiles(self, count_tiles: int) -> None:
        TILE_LIMIT = 99 * 99

        try:
            count_tiles = int(count_tiles)
        except ValueError:
            raise ValueError("count_tiles could not be cast to integer.")

        if count_tiles > TILE_LIMIT or count_tiles < 2:
            raise ValueError(
                f'Number of tiles must be between 2 and {TILE_LIMIT} (you asked for {count_tiles}).'
            )

    def __get_tile_size(self, rows: int, columns: int) -> tuple[int, int]:
        tile_width = int(floor(self.width / columns))
        tile_height = int(floor(self.height / rows))
        return tile_height, tile_width
