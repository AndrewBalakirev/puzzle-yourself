#!/usr/bin/env python
"""
puzzle yourself
"""
__version__ = '0.0.1'

import os
from pathlib import Path

from kivy.uix.screenmanager import ScreenManager
from kivymd.app import MDApp

from src.View.screens import screens


class PuzzleYourselfApp(MDApp):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.load_all_kv_files()
        self.screen_manager = ScreenManager()

    def build(self) -> ScreenManager:
        self.generate_application_screens()
        return self.screen_manager

    def generate_application_screens(self) -> None:
        for name_screen in screens.keys():
            model = screens[name_screen]["model"]()
            controller = screens[name_screen]["controller"](model)
            view = controller.get_view()
            view.name = name_screen
            self.screen_manager.add_widget(view)

    def load_all_kv_files(self) -> None:
        base_dir = os.path.dirname(os.path.abspath(__file__))
        src_dir = os.path.join(base_dir, 'src')
        for kv_file in Path(src_dir).rglob('*.kv'):
            self.load_kv(str(kv_file))


if __name__ == '__main__':
    app = PuzzleYourselfApp()
    app.run()
